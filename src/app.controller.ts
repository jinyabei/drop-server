import { Controller, Get } from '@nestjs/common';
import { UserService } from './modules/user/user.service';
import { User } from './modules/user/entities/user.entity';

@Controller()
export class AppController {
  constructor(private readonly userService: UserService) {}

  @Get('/create')
  async create(): Promise<boolean> {
    return await this.userService.create({
      name: '水滴超级管理员',
      desc: '管理员',
      tel: '8800888',
      password: '123456',
      account: 'admin',
    });
  }

  @Get('/del')
  async del(): Promise<boolean> {
    return await this.userService.del('1c987296-9332-4980-b3bb-45019ab4b03e');
  }

  @Get('/update')
  async update(): Promise<boolean> {
    return await this.userService.update(
      'dea69ec9-519d-4724-818c-e9dba6ef51f3',
      {
        name: '水滴超级管理员111111',
      },
    );
  }

  @Get('/find')
  async find(): Promise<User> {
    return await this.userService.find('dea69ec9-519d-4724-818c-e9dba6ef51f3');
  }
}
